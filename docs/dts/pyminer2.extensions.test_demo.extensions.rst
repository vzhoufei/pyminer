pyminer2.extensions.test\_demo.extensions package
=================================================

Subpackages
-----------

.. toctree::
   :maxdepth: 4

   pyminer2.extensions.test_demo.extensions.test_extension
   pyminer2.extensions.test_demo.extensions.test_extension2

Module contents
---------------

.. automodule:: pyminer2.extensions.test_demo.extensions
   :members:
   :undoc-members:
   :show-inheritance:
