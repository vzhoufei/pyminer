from pyminer2.ui.common.open_process_with_pyqt import PMGProcessConsoleWidget
from PyQt5.QtWidgets import QTabWidget


class PMProcessConsoleTabWidget(QTabWidget):
    def __init__(self, parent=None):
        super(PMProcessConsoleTabWidget, self).__init__(parent)

    def create_process(self,text :str,args:list,auto_run=True):
        w = PMGProcessConsoleWidget(args)
        if auto_run:
            w.process_console.start_process()
        self.addTab(w,text)
