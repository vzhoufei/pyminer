#!/usr/bin/env python
# -*- coding: utf-8 -*-

import sys
import os

sys.path.append(os.path.dirname(__file__))

from pyminer2.extensions.extensionlib import BaseInterface, BaseExtension
from .codeeditor.tabwidget import PMCodeEditTabWidget
from .toolbar import PMEditorToolbar


class Extension(BaseExtension):
    def on_loading(self):
        self.extension_lib.Program.add_translation('zh_CN', {'Editor': '编辑器'})

    def on_load(self):
        self.widgets['EditorsWidget'].extension_lib = self.extension_lib

    def on_install(self):
        pass

    def on_uninstall(self):
        pass


class Interface(BaseInterface):
    pass


class EditorToolBar(PMEditorToolbar):
    pass


class EditorsWidget(PMCodeEditTabWidget):
    pass
