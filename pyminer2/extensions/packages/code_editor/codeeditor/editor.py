#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
Created on 2020/9/7
@author: Irony
@email: 892768447@qq.com
@file: editor
@description: Code Editor
"""

__version__ = '0.1'

import logging
import os
import re
from itertools import groupby
from pathlib import Path

from PyQt5.Qsci import QsciLexerPython, QsciScintilla, QsciAPIs
from PyQt5.QtCore import QCoreApplication, Qt, QPoint, QDir, QEvent, QObject
from PyQt5.QtGui import QFont, QColor, QKeySequence
from PyQt5.QtWidgets import QWidget, QFileDialog, QMessageBox, QAction, QShortcut, QDialog, QVBoxLayout, QPushButton, \
    QHBoxLayout
from yapf.yapflib import py3compat, yapf_api

from codeeditor.tools import Utilities
from codeeditor.ui.ui_formeditor import Ui_FormEditor
# TODO to remove (use extensionlib)
from pyminer2.extensions.extensionlib.pmext import PluginInterface
from pmgwidgets import SettingsPanel

logger = logging.getLogger(__name__)


class FindDialog(QDialog):
    def __init__(self, parent=None, text_edit: 'PMCodeEditor' = None):
        super(FindDialog, self).__init__(parent)
        self.text_editor = text_edit
        self.qsci_text_edit: 'QsciScintilla' = text_edit.textEdit
        views = [(str, 'text_to_find', self.tr('Text to Find'), ''),
                 (str, 'text_to_replace', self.tr('Text to Replace'), ''),
                 (bool, 'wrap', self.tr('Wrap'), True),
                 (bool, 'regex', self.tr('Regex'), False),
                 (bool, 'case_sensitive', self.tr('Case Sensitive'), True),
                 (bool, 'whole_word', self.tr('Whole Word'), True),
                 ]
        self.settings_panel = SettingsPanel(views)
        self.setLayout(QVBoxLayout())
        self.layout().addWidget(self.settings_panel)
        self.button_up = QPushButton('up')
        self.button_down = QPushButton('dn')
        self.button_replace = QPushButton('replace')
        self.button_replace_all = QPushButton('replace all')

        self.button_up.clicked.connect(self.search_up)
        self.button_down.clicked.connect(self.search_down)
        self.button_replace.clicked.connect(self.replace_current)
        self.button_replace_all.clicked.connect(self.replace_all)

        self.button_bar = QHBoxLayout()
        self.button_bar.addWidget(self.button_up)
        self.button_bar.addWidget(self.button_down)
        self.button_bar.addWidget(self.button_replace)
        self.button_bar.addWidget(self.button_replace_all)
        self.button_bar.setContentsMargins(0, 0, 0, 0)
        self.layout().addLayout(self.button_bar)

    def search_up(self):
        settings = self.settings_panel.get_value()
        self.text_editor.search_word(forward=True, **settings)
        pass

    def search_down(self):
        '''
        反方向查找。注意，简单的设置qsci的forward=False是不够的，还需要对位置进行处理。
        这似乎是QSciScintilla的bug.
        '''
        settings = self.settings_panel.get_value()
        line, index = self.text_editor.textEdit.getSelection()[:2]
        self.text_editor.search_word(forward=False, **settings, line=line, index=index)
        pass

    def replace_current(self):
        text: str = self.settings_panel.widgets_dic['text_to_replace'].get_value()
        if self.qsci_text_edit.hasSelectedText():
            self.qsci_text_edit.replace(text)

    def replace_all(self):
        settings = self.settings_panel.get_value()
        text_to_replace = self.settings_panel.widgets_dic['text_to_replace'].get_value()
        while (1):
            b = self.text_editor.search_word(forward=True, **settings)
            if b:
                self.qsci_text_edit.replace(text_to_replace)
            else:
                break


class PMCodeEditor(QWidget, Ui_FormEditor):
    """
    自定义编辑器控件
    """

    def __init__(self, *args, **kwargs):
        super(PMCodeEditor, self).__init__(*args, **kwargs)
        self._parent = self.parent()
        self.setupUi(self)
        self._lexer = None
        self._apis = None
        self._path = ''
        self._encoding = 'utf-8'
        self._action_format = None  # 格式化
        self._action_run_sel_code = None  # 运行选中代码
        self._action_run_code = None  # 运行代码
        self._shortcut_format = None
        self._shortcut_run = None
        self._indicator_error = -1
        self._indicator_error2 = -1
        self._indicator_warn = -1
        self._indicator_info = -1
        self._indicator_dict = {}  # 指示器记录
        self._init_editor()
        self._init_lexer()
        self._init_apis()
        self._init_actions()
        self._init_signals()

        self.last_hint = ''
        self.find_dialog: 'FindDialog' = None

    def text(self, selected: bool = False) -> str:
        """
        返回编辑器选中或者全部内容

        :rtype: str
        :return: 返回编辑器选中或者全部内容
        """
        if selected:
            return self.textEdit.selectedText()
        return self.textEdit.text()

    def set_text(self, text: str) -> None:
        """
        设置编辑器内容

        :type text: str
        :param text: 文本内容
        :return: None
        """
        # self.textEdit.setText(text)  # 该方法会重置撤销历史
        try:
            text = text.encode(self._encoding)
        except Exception as e:
            logger.warning(str(e))
            text = text.encode('utf-8', errors='ignore')
        self.textEdit.SendScintilla(QsciScintilla.SCI_SETTEXT, text)

    def filename(self) -> str:
        """
        返回当前文件名

        :rtype: str
        :return: 返回当前文件名
        """
        return os.path.basename(self._path)

    def path(self) -> str:
        """
        返回当前文件路径

        :rtype: str
        :return: 返回当前文件路径
        """
        return self._path

    def set_path(self, path: str) -> None:
        """
        设置文件路径

        :param path: 设置文件路径
        :type path: str
        :return: None
        """
        self._path = path

    def modified(self) -> bool:
        """
        返回内容是否被修改

        :rtype: bool
        :return: 返回内容是否被修改
        """
        return self.textEdit.isModified()

    def set_modified(self, modified: bool) -> None:
        """
        设置内容是否被修改

        :param modified: 是否被修改 True or False
        :type: bool
        :return: None
        """
        self.textEdit.setModified(modified)

    def set_encoding(self, encoding: str):
        """
        设置文本编码，仅支持 ASCII 和 UTF-8

        :param encoding: ascii or gbk or utf-8
        :type: str
        :return:
        """
        encoding = encoding.lower()
        self._encoding = encoding
        self.label_status_encoding.setText(encoding.upper())
        if encoding.startswith('utf'):
            self.textEdit.setUtf8(True)
            self.textEdit.SendScintilla(QsciScintilla.SCI_SETCODEPAGE, QsciScintilla.SC_CP_UTF8)
        else:
            self.textEdit.setUtf8(False)
            self.textEdit.SendScintilla(QsciScintilla.SCI_SETCODEPAGE, 936)

    def load_file(self, path: str) -> None:
        """
        加载文件

        :param path: 文件路径
        :type path: str
        :return: None
        """
        self._path = ''
        try:
            # 读取文件内容并加载
            with open(path, 'rb') as fp:
                text = fp.read()
                text, coding = Utilities.decode(text)
                self.set_encoding(coding)
                self.set_text(text)
                self.set_modified(False)
                self.set_eol_status()
        except Exception as e:
            logger.warning(str(e))
            PluginInterface.show_log('error', 'CodeEditor', str(e))
        self._path = path
        self.setWindowTitle(self.filename())

    def set_indicators(self, msgs, clear=True):
        """
        设置 error warning info 指示器

        :param msgs: 消息数组
        :param clear: 是否清空之前的标记
        :type msgs: Options[list, str]
        :type clear: bool
        :return:
        """
        # 清除所有效果
        if clear:
            self.textEdit.clearAnnotations()
            lines = self.textEdit.lines()
            columns = self.textEdit.lineLength(lines - 1)
            self.textEdit.clearIndicatorRange(0, 0, lines, columns, self._indicator_error)
            self.textEdit.clearIndicatorRange(0, 0, lines, columns, self._indicator_warn)
            self.textEdit.clearIndicatorRange(0, 0, lines, columns, self._indicator_info)
        if not isinstance(msgs, (tuple, list)):
            return
        for msg in msgs:
            s_msg = msg.split(':')
            if len(s_msg) < 4:
                continue
            from_line, from_col, n_type = s_msg[:3]
            from_line, from_col = int(from_line) - 1, int(from_col) - 1
            # msg = ''.join(s_msg[3:])
            # try:
            #     matched = re.match(r'\s+["\'](.*?)["\']\s+', msg)  # 匹配可能出现的文字
            #     to_col = from_col + matched.end() - matched.start()
            # except Exception as e:
            #     logger.debug(str(e))
            to_col = len(self.textEdit.text(from_line))
            number = self._indicator_error if n_type.startswith('E') else self._indicator_warn if n_type.startswith(
                'W') else self._indicator_info
            self._indicator_dict[from_line] = to_col
            print(from_line, 0, from_line, to_col, number)
            self.textEdit.fillIndicatorRange(from_line, 0, from_line, to_col, number)

    def set_eol_status(self):
        """
        根据文件内容中的换行符设置底部状态

        :return:
        """
        eols = re.findall(r'\r\n|\r|\n', self.text())
        if not eols:
            self.label_status_eol.setText('Unix(LF)')
            self.textEdit.setEolMode(QsciScintilla.EolUnix)  # \n换行
            return
        grouped = [(len(list(group)), key) for key, group in groupby(sorted(eols))]
        eol = sorted(grouped, reverse=True)[0][1]
        if eol == '\r\n':
            self.label_status_eol.setText('Windows(CR LF)')
            self.textEdit.setEolMode(QsciScintilla.EolWindows)  # \r\n换行
            return QsciScintilla.EolWindows
        if eol == '\r':
            self.label_status_eol.setText('Mac(CR)')
            self.textEdit.setEolMode(QsciScintilla.EolMac)  # \r换行
            return
        self.label_status_eol.setText('Unix(LF)')
        self.textEdit.setEolMode(QsciScintilla.EolUnix)  # \n换行

    def _init_editor(self) -> None:
        """
        初始化编辑器设置

        :return: None
        """
        self.label_status_ln_col.setText(self.tr('Ln:1  Col:1'))
        self.label_status_length.setText(self.tr('Length:0  Lines:1'))
        self.label_status_sel.setText(self.tr('Sel:0 | 0'))
        self.textEdit.setContextMenuPolicy(Qt.CustomContextMenu)
        # 设置字体
        self.textEdit.setFont(QFont('Source Code Pro', 12))  # Consolas
        self.textEdit.setMarginsFont(self.textEdit.font())
        # 自动换行
        self.textEdit.setEolMode(QsciScintilla.EolUnix)  # \n换行
        self.textEdit.setWrapMode(QsciScintilla.WrapWord)  # 自动换行
        self.textEdit.setWrapVisualFlags(QsciScintilla.WrapFlagNone)
        self.textEdit.setWrapIndentMode(QsciScintilla.WrapIndentFixed)
        # 编码
        self.textEdit.setUtf8(True)
        self.textEdit.SendScintilla(QsciScintilla.SCI_SETCODEPAGE, QsciScintilla.SC_CP_UTF8)
        # 自动提示
        self.textEdit.setAnnotationDisplay(QsciScintilla.AnnotationBoxed)  # 提示显示方式
        self.textEdit.setAutoCompletionSource(QsciScintilla.AcsAll)  # 自动补全。对于所有Ascii字符
        self.textEdit.setAutoCompletionReplaceWord(True)
        self.textEdit.setAutoCompletionCaseSensitivity(False)  # 忽略大小写
        # self.textEdit.setAutoCompletionUseSingle(QsciScintilla.AcusAlways)
        self.textEdit.setAutoCompletionThreshold(1)  # 输入多少个字符才弹出补全提示
        self.textEdit.setCallTipsPosition(QsciScintilla.CallTipsBelowText)  # 设置提示位置
        self.textEdit.setCallTipsStyle(QsciScintilla.CallTipsNoContext)  # 设置提示样式
        # 设置折叠样式
        self.textEdit.setFolding(QsciScintilla.FoldStyle.BoxedTreeFoldStyle)  # 代码折叠
        self.textEdit.setFoldMarginColors(QColor(233, 233, 233), Qt.white)
        # 折叠标签颜色
        self.textEdit.SendScintilla(QsciScintilla.SCI_MARKERSETBACK, QsciScintilla.SC_MARKNUM_FOLDERSUB,
                                    QColor('0xa0a0a0'))
        self.textEdit.SendScintilla(QsciScintilla.SCI_MARKERSETBACK, QsciScintilla.SC_MARKNUM_FOLDERMIDTAIL,
                                    QColor('0xa0a0a0'))
        self.textEdit.SendScintilla(QsciScintilla.SCI_MARKERSETBACK, QsciScintilla.SC_MARKNUM_FOLDERTAIL,
                                    QColor('0xa0a0a0'))
        # 设置当前行背景
        self.textEdit.setCaretLineVisible(True)
        self.textEdit.setCaretLineBackgroundColor(QColor(232, 232, 255))

        # 设置选中文本颜色
        # self.textEdit.setSelectionForegroundColor(QColor(192, 192, 192))
        # self.textEdit.setSelectionBackgroundColor(QColor(192, 192, 192))

        # 括号匹配
        self.textEdit.setBraceMatching(QsciScintilla.StrictBraceMatch)  # 大括号严格匹配
        self.textEdit.setMatchedBraceBackgroundColor(Qt.blue)
        self.textEdit.setMatchedBraceForegroundColor(Qt.white)
        self.textEdit.setUnmatchedBraceBackgroundColor(Qt.red)
        self.textEdit.setUnmatchedBraceForegroundColor(Qt.white)

        # 启用活动热点区域的下划线
        self.textEdit.setHotspotUnderline(True)
        self.textEdit.setHotspotWrap(True)

        # 缩进
        self.textEdit.setAutoIndent(True)  # 换行后自动缩进
        self.textEdit.setTabWidth(4)
        self.textEdit.setIndentationWidth(4)
        self.textEdit.setTabIndents(True)
        # 缩进指南
        self.textEdit.setIndentationGuides(True)
        self.textEdit.setIndentationsUseTabs(False)  # 不使用Tab
        self.textEdit.setBackspaceUnindents(True)  # 当一行没有其它字符时删除前面的缩进
        self.textEdit.setIndentationGuidesForegroundColor(QColor(192, 192, 192))
        self.textEdit.setIndentationGuidesBackgroundColor(Qt.white)

        # 显示行号
        self.textEdit.setMarginLineNumbers(0, True)
        self.textEdit.setMarginWidth(0, 50)
        self.textEdit.setMarginWidth(1, 0)  # 行号
        #  self.textEdit.setMarginWidth(2, 0)  # 折叠
        self.textEdit.setMarginWidth(3, 0)
        self.textEdit.setMarginWidth(4, 0)
        #  # 折叠区域
        #  self.textEdit.setMarginType(3, QsciScintilla.SymbolMargin)
        #  self.textEdit.setMarginLineNumbers(3, False)
        #  self.textEdit.setMarginWidth(3, 15)
        #  self.textEdit.setMarginSensitivity(3, True)

        # 设置空白字符显示
        self.textEdit.setWhitespaceSize(1)  # 可见的空白点的尺寸
        self.textEdit.setWhitespaceVisibility(QsciScintilla.WsVisible)  # 空白的可见性。默认的是空格是无形的
        self.textEdit.setWhitespaceForegroundColor(QColor(255, 181, 106))

        # 设置右边边界线
        self.textEdit.setEdgeColumn(120)
        self.textEdit.setEdgeMode(QsciScintilla.EdgeLine)

        # 设置代码检测后波浪线
        self._indicator_error = self.textEdit.indicatorDefine(QsciScintilla.SquigglePixmapIndicator)
        self._indicator_error2 = self.textEdit.indicatorDefine(QsciScintilla.SquigglePixmapIndicator)
        self._indicator_warn = self.textEdit.indicatorDefine(QsciScintilla.SquigglePixmapIndicator)
        self._indicator_info = self.textEdit.indicatorDefine(QsciScintilla.SquigglePixmapIndicator)
        self.textEdit.setIndicatorForegroundColor(QColor(Qt.red), self._indicator_error)
        self.textEdit.setIndicatorForegroundColor(QColor(Qt.red), self._indicator_error2)
        self.textEdit.setIndicatorForegroundColor(QColor(244, 152, 16), self._indicator_warn)
        self.textEdit.setIndicatorForegroundColor(QColor(Qt.green), self._indicator_info)

        # 鼠标跟踪
        self.textEdit.viewport().setMouseTracking(True)
        # # 安装键盘过滤器
        # self.textEdit.installEventFilter(self)
        # 安装鼠标移动过滤器
        self.textEdit.viewport().installEventFilter(self)

    def _init_lexer(self) -> None:
        """
        初始化语法解析器

        :return: None
        """
        self._lexer = QsciLexerPython(self.textEdit)
        self._lexer.setFont(self.textEdit.font())
        self.textEdit.setLexer(self._lexer)

    def _init_apis(self) -> None:
        """
        加载自定义智能提示文件

        :return: None
        """
        self._apis = QsciAPIs(self._lexer)
        for path in Path(os.path.join(os.path.dirname(__file__), 'api')).rglob('*.api'):
            logger.info('load %s' % str(path.absolute()))
            self._apis.load(str(path.absolute()))
        try:
            # 添加额外关键词
            for word in self._parent.keywords():
                self._apis.add(word)
        except Exception as e:
            logger.warning(str(e))
        self._apis.prepare()

    def _init_actions(self) -> None:
        """
        初始化额外菜单项

        :return:
        """
        self._action_format = QAction(self.tr('Format Code'), self.textEdit)
        self._action_run_code = QAction(self.tr('Run Code'), self.textEdit)
        self._action_run_sel_code = QAction(self.tr('Run Selected Code'), self.textEdit)
        self._action_save = QAction(self.tr('Save'), self.textEdit)
        self._action_find_replace = QAction(self.tr('Find/Replace'), self.textEdit)
        # 设置快捷键
        self._shortcut_format = QShortcut(QKeySequence('Ctrl+Alt+F'), self.textEdit)
        self._action_format.setShortcut(QKeySequence('Ctrl+Alt+F'))

        self._shortcut_run = QShortcut(QKeySequence('Ctrl+R'), self.textEdit)
        self._action_run_code.setShortcut(QKeySequence('Ctrl+R'))

        self._action_save.setShortcut(QKeySequence('Ctrl+S'))
        self._shortcut_save = QShortcut(QKeySequence('Ctrl+S'), self.textEdit)

        self._action_find_replace.setShortcut(QKeySequence('Ctrl+F'))
        self._shortcut_find_replace = QShortcut(QKeySequence('Ctrl+F'), self.textEdit)

    def on_textedit_focusin(self, e):
        QsciScintilla.focusInEvent(self.textEdit, e)
        self.extension_lib.UI.switch_toolbar('code_editor_toolbar', switch_only=True)

    def _init_signals(self) -> None:
        """
        初始化信号绑定

        :return: None
        """

        # 绑定获得焦点信号
        self.textEdit.focusInEvent = self.on_textedit_focusin
        # 绑定光标变化信号
        self.textEdit.cursorPositionChanged.connect(self.slot_cursor_position_changed)
        # 绑定内容改变信号
        self.textEdit.textChanged.connect(self.slot_text_changed)
        # 绑定选中变化信号
        self.textEdit.selectionChanged.connect(self.slot_selection_changed)
        # 绑定是否被修改信号
        self.textEdit.modificationChanged.connect(self.slot_modification_changed)
        # 绑定右键菜单信号
        self.textEdit.customContextMenuRequested.connect(self.slot_custom_context_menu_requested)
        # 绑定快捷键信号
        self._action_format.triggered.connect(self.slot_code_format)
        self._shortcut_format.activated.connect(self.slot_code_format)
        self._action_run_code.triggered.connect(self.slot_code_run)
        self._shortcut_run.activated.connect(self.slot_code_run)

        self._shortcut_save.activated.connect(self.slot_save)
        self._action_save.triggered.connect(self.slot_save)

        self._action_find_replace.triggered.connect(self.slot_find_or_replace)
        self._shortcut_find_replace.activated.connect(self.slot_find_or_replace)

    def slot_cursor_position_changed(self, line: int, column: int) -> None:
        """
        光标变化槽函数

        :param line: 行
        :param column: 列
        :type line: int
        :type column: int
        :return: None
        """
        self.label_status_ln_col.setText(
            self.tr('Ln:{0}  Col:{1}').format(format(line + 1, ','), format(column + 1, ',')))

    def get_hint(self):
        pos = self.textEdit.getCursorPosition()
        text = self.textEdit.text(pos[0])
        try:
            line = text[:pos[1]+1]
        except Exception as e:
            logger.debug(e)
            line = ''
        hint: str = re.split(r'[;,:\./ \\!&\|\*\+\s\(\)\{\}\[\]]', line)[-1].strip()
        return hint

    def update_api(self, hint: str, pos: tuple):
        l = []
        if len(self.last_hint) > len(hint) or hint == '':
            self.last_hint = hint
            return
        self.last_hint = hint

        import jedi
        path = self._path.replace(os.sep, '/')
        if path.startswith(QDir.tempPath().replace(os.sep, '/')):
            path = ''
        else:
            path = self._path
        script = jedi.Script(code=self.textEdit.text(), path=path)
        completions = script.complete(pos[0] + 1, pos[1])
        for c in completions:
            l.append(c.complete)
        self._apis = QsciAPIs(self._lexer)
        for s in l:
            self._apis.add(s)
        self._apis.prepare()

    def slot_text_changed(self) -> None:
        """
        内容变化槽函数

        :return: None
        """
        pos = self.textEdit.getCursorPosition()
        hint = self.get_hint()
        if (not hint.startswith(self.last_hint)) or self.last_hint == '':
            self.update_api(hint, pos)

        self.label_status_length.setText(self.tr('Length:{0}  Lines:{1}').format(format(self.textEdit.length(), ','),
                                                                                 format(self.textEdit.lines(), ',')))
        self.slot_modification_changed(True)
        self.set_modified(True)

    def slot_selection_changed(self) -> None:
        """
        选中内容变化槽函数

        :return: None
        """
        line_from, index_from, line_to, index_to = self.textEdit.getSelection()
        lines = 0 if line_from == line_to == -1 else line_to - line_from + 1
        self.label_status_sel.setText(
            self.tr('Sel:{0} | {1}').format(format(len(self.textEdit.selectedText()), ','), format(lines, ',')))

    def slot_modification_changed(self, modified: bool) -> None:
        """
        内容被修改槽函数

        :param modified: 是否被修改
        :type modified: bool
        :return:
        """
        title = self.windowTitle()
        if modified:
            if not title.startswith('*'):
                self.setWindowTitle('*' + title)
        else:
            if title.startswith('*'):
                self.setWindowTitle(title[1:])

    def slot_find_or_replace(self):
        if self.find_dialog is None:
            self.find_dialog = FindDialog(parent=self, text_edit=self)
        self.find_dialog.show()
        return
        match_regex = False
        case_sensitive = False
        match_whole_word = False
        wrap_find = False

        first = self.textEdit.findFirst('def', False, False, False, False)
        self.textEdit.replace('ggg')

    def search_word(self, text_to_find: bool, wrap: bool, regex: bool, case_sensitive: bool, whole_word: bool,
                    forward: bool, index=-1, line=-1, **kwargs):

        return self.textEdit.findFirst(text_to_find, regex, case_sensitive, whole_word, wrap, forward, line, index)

    def slot_custom_context_menu_requested(self, pos: QPoint) -> None:
        """
        右键菜单修改

        :param pos:
        :type pos: QPoint
        :return: None
        """
        # 创建默认菜单
        menu = self.textEdit.createStandardContextMenu()
        # 遍历本身已有的菜单项做翻译处理
        # 前提是要加载了Qt自带的翻译文件
        for action in menu.actions():
            action.setText(QCoreApplication.translate('QTextControl', action.text()))
        # 添加额外菜单
        menu.addSeparator()
        menu.addAction(self._action_format)
        menu.addAction(self._action_run_code)
        menu.addAction(self._action_run_sel_code)
        menu.addAction(self._action_save)
        menu.addAction(self._action_find_replace)
        # 根据条件决定菜单是否可用
        enabled = len(self.text().strip()) > 0
        self._action_format.setEnabled(enabled)
        self._action_run_code.setEnabled(enabled)
        self._action_run_sel_code.setEnabled(self.textEdit.hasSelectedText())
        menu.exec_(self.textEdit.mapToGlobal(pos))
        del menu

    def slot_save(self) -> bool:
        """
        保存当前文件

        :rtype: bool
        :return: 保存成功或失败
        """
        # 保存文件

        path = self._path.replace(os.sep, '/')
        if path.startswith(QDir.tempPath().replace(os.sep, '/')):
            # 弹出对话框要求选择真实路径保存
            path, ext = QFileDialog.getSaveFileName(self, self.tr('Save file'), PluginInterface.get_root_dir(),
                                                    filter='*.py')
            ext = '.py'

            if not path:
                return False
            if not path.endswith(ext):
                path += ext
            self._path = path
        try:
            with open(self._path, 'wb') as fp:
                fp.write(self.text().encode('utf-8', errors='ignore'))

            self.setWindowTitle(os.path.basename(path))
            self.slot_modification_changed(False)
            self.set_modified(False)
            return True
        except Exception as e:
            # 保存失败
            logger.warning(str(e))
        return False

    def slot_about_close(self, save_all=False) -> QMessageBox.StandardButton:
        """
        是否需要关闭以及保存

        :param save_all: 当整个窗口关闭时增加是否全部关闭
        :return:QMessageBox.StandardButton
        """
        if not self.modified():
            return QMessageBox.Discard
        buttons = QMessageBox.Save | QMessageBox.Discard | QMessageBox.Cancel
        if save_all:
            buttons |= QMessageBox.SaveAll  # 保存全部
            buttons |= QMessageBox.NoToAll  # 放弃所有
        ret = QMessageBox.question(self, self.tr('Save'), self.tr('Save file "{0}"?').format(self.filename()), buttons,
                                   QMessageBox.Save)
        if ret == QMessageBox.Save or ret == QMessageBox.SaveAll:
            if not self.slot_save():
                return QMessageBox.Cancel
        return ret

    def slot_code_format(self):
        """
        格式化代码

        :return:
        """
        text = self.text().strip()
        if not text:
            return
        text = py3compat.removeBOM(text)
        try:
            reformatted_source, _ = yapf_api.FormatCode(
                text,
                filename=self.filename(),
                # print_diff=True,
                style_config=os.path.join(os.path.dirname(__file__), 'config', '.style.yapf')
            )
            self.set_text(reformatted_source)
        except Exception as e:
            logger.warning(str(e))
            lines = re.findall(r'line (\d+)\)', str(e))
            row = -1
            if lines:
                # 跳转到指定行
                row = int(lines[0])
                row = row - 1 if row else 0
                col = self.textEdit.lineLength(row)
                self.textEdit.setCursorPosition(row, col - 1)
                # 标记波浪线
                self.textEdit.fillIndicatorRange(row, 0, row, col, self._indicator_error2)
            QMessageBox.critical(self, self.tr('Error'), str(e))
            if row > -1:
                # 清除被标记波浪线
                self.textEdit.clearIndicatorRange(row, 0, row, col, self._indicator_error2)

    def slot_code_sel_run(self):
        """
        运行选中代码

        :return:
        """
        text = self.text(True).strip()
        if not text:
            return
        try:
            self._parent.slot_run_script(text)
        except Exception as e:
            logger.warning(str(e))

    def slot_code_run(self):
        """
        运行代码

        :return:
        """
        print('run code', self._parent)
        text = self.text().strip()
        if not text:
            return
        try:
            self._parent.slot_run_script(text)
        except Exception as e:
            logger.warning(str(e))

    def eventFilter(self, obj: 'QObject', event: 'QEvent') -> bool:
        # if event.type() == QEvent.MouseMove:
        #     # 如果有错误则显示详情
        #     print(self.textEdit.lineAt(event.pos()))
        return False
