<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="en">
<context>
    <name>FormEditor</name>
    <message>
        <location filename="../ui/formeditor.ui" line="14"/>
        <source>Form</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../ui/formeditor.ui" line="58"/>
        <source>UTF-8</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../ui/formeditor.ui" line="79"/>
        <source>Unix(LF)</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../ui/formeditor.ui" line="51"/>
        <source>Length:{0}  Lines:{1}</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../ui/formeditor.ui" line="65"/>
        <source>Sel:{0} | {1}</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../ui/formeditor.ui" line="72"/>
        <source>Ln:{0}  Col:{1}</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>PMCodeEditTabWidget</name>
    <message>
        <location filename="../tabwidget.py" line="255"/>
        <source>Open File</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../tabwidget.py" line="368"/>
        <source>Run: %s</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>PMCodeEditor</name>
    <message>
        <location filename="../editor.py" line="248"/>
        <source>Ln:1  Col:1</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../editor.py" line="249"/>
        <source>Length:0  Lines:1</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../editor.py" line="250"/>
        <source>Sel:0 | 0</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../editor.py" line="386"/>
        <source>Format Code</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../editor.py" line="387"/>
        <source>Run Code</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../editor.py" line="388"/>
        <source>Run Selected Code</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../editor.py" line="427"/>
        <source>Ln:{0}  Col:{1}</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../editor.py" line="436"/>
        <source>Length:{0}  Lines:{1}</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../editor.py" line="447"/>
        <source>Sel:{0} | {1}</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../editor.py" line="504"/>
        <source>Save file</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../editor.py" line="532"/>
        <source>Save</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../editor.py" line="532"/>
        <source>Save file &quot;{0}&quot;?</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../editor.py" line="569"/>
        <source>Error</source>
        <translation></translation>
    </message>
</context>
</TS>
