# -*- coding: utf-8 -*-
# @Time    : 2020/9/4 10:29
# @Author  : 别着急慢慢来
# @FileName: PMAgg.py


import os
import numpy as np
from PyQt5 import QtWidgets
from PyQt5 import QtCore
from PyQt5.QtGui import QIcon, QCursor
from matplotlib._pylab_helpers import Gcf
from matplotlib.backend_bases import FigureManagerBase
from matplotlib.backends.backend_qt5 import NavigationToolbar2QT as NavigationToolbar
from matplotlib.backends.backend_qt5agg import FigureCanvasQTAgg as FigureCanvas
from matplotlib.figure import Figure
import sys
from ui.pmagg_ui import Ui_MainWindow
from ui.axes_control_manager import Ui_Form_Manager
from ui import linestyle_manager
import matplotlib.pyplot as plt
from matplotlib.patches import Ellipse, Rectangle
from PyQt5.QtWidgets import QAction, QWidget, QSizePolicy, QMenu, QApplication, QHBoxLayout, QFormLayout
from matplotlib.lines import Line2D
from matplotlib.text import Annotation, Text
from matplotlib.legend import Legend
from matplotlib.font_manager import FontProperties
from ui.value_inputs import SettingsPanel
from ui.linestyles import *
from matplotlib.colors import to_hex, to_rgb, to_rgba
import matplotlib.font_manager
from matplotlib import rcParams
from pathlib import Path
import configparser
import re
from shutil import copyfile, move
import shutil

"""
前提，将该文件所在文件夹注册到path环境变量
1. 永久添加
2. 临时添加
import os
import sys
current_path=os.getcwd()
sys.path.append(current_path)
最好永久添加，这样脚本和控制台都能用


使用时，先申明
import matplotlib
matplotlib.use('module://pmagg')

然后就可以
plt.plot([1,2,3],[4,5,6])
plt.show()


Window类功能需求
1. 拖拽窗口，绘图区始终处于窗口中心，且窗口大小改变时，绘图区能跟着扩展
2. 实现对绘图区坐标轴，图例，标题，文字等各类信息的修改和添加
3. 实现绘图区参数的保存，方便下次调用
4. 实现图片保存
5. 模仿matlab实现更多的功能

Extension 使用时被加载
"""


class Window(QtWidgets.QMainWindow, Ui_MainWindow):
    """
    重写修改该类即可实现自定义后端界面，相加什么按钮可以随便加，目前还只是个demo

    self.canvas.draw() 每执行该函数，图形重绘
    """

    def __init__(self, figure):
        super(Window, self).__init__()
        self.setupUi(self)  # 先执行父类方法，以产生成员变量
        self.figure = figure
        self.canvas = FigureCanvas(self.figure)  # 这里的canvas就是曲线图
        self.toolbar = NavigationToolbar(self.canvas, self)
        self.toolbar.hide()  # 隐藏QT原来的工具栏
        self.gridLayout.addWidget(self.canvas)
        # 槽函数连接
        # 当前子图对象切换
        self.current_path = os.path.dirname(__file__)
        self.saveAction = QAction(QIcon(os.path.join(self.current_path, 'icons/save.png')), 'save', self)
        self.saveAction.setShortcut('Ctrl+S')
        self.saveAction.triggered.connect(self.save_slot)
        self.toolBar.addAction(self.saveAction)

        self.settingAction = QAction(QIcon(os.path.join(self.current_path, 'icons/setting.png')), 'setting', self)
        self.settingAction.triggered.connect(self.axes_control_slot)
        self.toolBar.addAction(self.settingAction)

        self.homeAction = QAction(QIcon(os.path.join(self.current_path, 'icons/home.png')), 'home', self)
        self.homeAction.setShortcut('Ctrl+H')
        self.homeAction.triggered.connect(self.home_slot)
        self.toolBar.addAction(self.homeAction)

        self.backAction = QAction(QIcon(os.path.join(self.current_path, 'icons/back.png')), 'back', self)
        self.backAction.triggered.connect(self.back_slot)
        self.toolBar.addAction(self.backAction)

        self.frontAction = QAction(QIcon(os.path.join(self.current_path, 'icons/front.png')), 'front', self)
        self.frontAction.triggered.connect(self.front_slot)
        self.toolBar.addAction(self.frontAction)

        self.zoomAction = QAction(QIcon(os.path.join(self.current_path, 'icons/zoom.png')), 'zoom', self)
        self.zoomAction.triggered.connect(self.zoom_slot)
        self.toolBar.addAction(self.zoomAction)

        self.panAction = QAction(QIcon(os.path.join(self.current_path, 'icons/pan.png')), '平移', self)
        self.panAction.triggered.connect(self.pan_slot)
        self.toolBar.addAction(self.panAction)

        self.rotateAction = QAction(QIcon(os.path.join(self.current_path, 'icons/rotate.png')), 'rotate', self)
        self.rotateAction.triggered.connect(self.rotate_slot)
        self.toolBar.addAction(self.rotateAction)

        self.textAction = QAction(QIcon(os.path.join(self.current_path, 'icons/text.png')), 'text', self)
        self.textAction.triggered.connect(self.add_text_slot)
        self.toolBar.addAction(self.textAction)

        self.rectAction = QAction(QIcon(os.path.join(self.current_path, 'icons/rect.png')), 'rect', self)
        self.rectAction.triggered.connect(self.add_rect_slot)
        self.toolBar.addAction(self.rectAction)

        self.ovalAction = QAction(QIcon(os.path.join(self.current_path, 'icons/oval.png')), 'oval', self)
        self.ovalAction.triggered.connect(self.add_oval_slot)
        self.toolBar.addAction(self.ovalAction)

        self.annotationAction = QAction(QIcon(os.path.join(self.current_path, 'icons/annotation.png')), 'annotation',
                                        self)
        self.annotationAction.triggered.connect(self.add_annotation_slot)
        self.toolBar.addAction(self.annotationAction)

        self.styleAction = QAction(QIcon(os.path.join(self.current_path, 'icons/style.png')), 'style', self)
        self.styleAction.triggered.connect(self.add_style_slot)
        self.toolBar.addAction(self.styleAction)

        self.gridAction = QAction(QIcon(os.path.join(self.current_path, 'icons/grid.png')), '显示/隐藏网格', self)
        self.gridAction.triggered.connect(self.show_grid_slot)
        self.toolBar.addAction(self.gridAction)

        self.legendAction = QAction(QIcon(os.path.join(self.current_path, 'icons/legend.png')), '显示/隐藏图例', self)
        self.legendAction.triggered.connect(self.show_legend_slot)
        self.toolBar.addAction(self.legendAction)

        self.colorbarAction = QAction(QIcon(os.path.join(self.current_path, 'icons/colorbar.png')), '显示/隐藏colorbar',
                                      self)
        self.colorbarAction.triggered.connect(self.show_colorbar_slot)
        self.toolBar.addAction(self.colorbarAction)

        self.layoutAction = QAction(QIcon(os.path.join(self.current_path, 'icons/layout.png')), '改变布局', self)
        # self.layoutAction.triggered.connect(self.show_layout_slot)
        self.toolBar.addAction(self.layoutAction)

        # 将下拉菜单放在最右边
        self.toolBar.addSeparator()
        spacer = QWidget()
        spacer.setSizePolicy(QSizePolicy.Expanding, QSizePolicy.Expanding)
        self.toolBar.addWidget(spacer)
        self.toolBar.addWidget(self.comboBox)

        # 以上为工具栏1

        self.mainViewAction = QAction(QIcon(os.path.join(self.current_path, 'icons/mainView.png')), 'mainView', self)
        self.mainViewAction.triggered.connect(self.mainView_slot)
        self.toolBar_2.addAction(self.mainViewAction)

        self.leftViewAction = QAction(QIcon(os.path.join(self.current_path, 'icons/leftView.png')), 'leftView', self)
        self.leftViewAction.triggered.connect(self.leftView_slot)
        self.toolBar_2.addAction(self.leftViewAction)

        self.rightViewAction = QAction(QIcon(os.path.join(self.current_path, 'icons/rightView.png')), 'rightView', self)
        self.rightViewAction.triggered.connect(self.rightView_slot)
        self.toolBar_2.addAction(self.rightViewAction)

        self.topViewAction = QAction(QIcon(os.path.join(self.current_path, 'icons/topView.png')), 'topView', self)
        self.topViewAction.triggered.connect(self.topView_slot)
        self.toolBar_2.addAction(self.topViewAction)

        self.bottomViewAction = QAction(QIcon(os.path.join(self.current_path, 'icons/bottomView.png')), 'bottomView',
                                        self)
        self.bottomViewAction.triggered.connect(self.bottomView_slot)
        self.toolBar_2.addAction(self.bottomViewAction)

        self.backViewAction = QAction(QIcon(os.path.join(self.current_path, 'icons/backView.png')), 'backView', self)
        self.backViewAction.triggered.connect(self.backView_slot)
        self.toolBar_2.addAction(self.backViewAction)

        # 将下拉菜单放在最右边
        self.toolBar_2.addSeparator()
        spacer = QWidget()
        spacer.setSizePolicy(QSizePolicy.Expanding, QSizePolicy.Expanding)
        self.toolBar_2.addWidget(spacer)
        self.toolBar_2.addWidget(self.label)

        # 样式右键菜单功能集
        self.setContextMenuPolicy(QtCore.Qt.CustomContextMenu)
        self.customContextMenuRequested.connect(self.rightMenuShow)

        self.comboBox.currentIndexChanged.connect(self.combobox_slot)

        # 鼠标拖拽，实现三维图形旋转功能
        self.canvas.mpl_connect('motion_notify_event', self.on_rotate)
        # 鼠标拖拽，实现画矩形功能
        self.canvas.mpl_connect('button_press_event', self.add_rect)
        self.canvas.mpl_connect('motion_notify_event', self.add_rect)
        self.canvas.mpl_connect('button_release_event', self.add_rect)
        self.canvas.mpl_connect('pick_event', self.add_rect)
        self.mouse_pressed = False
        # 鼠标拖拽，实现画椭圆的功能实现
        self.canvas.mpl_connect('button_press_event', self.add_oval)
        self.canvas.mpl_connect('motion_notify_event', self.add_oval)
        self.canvas.mpl_connect('button_release_event', self.add_oval)
        self.canvas.mpl_connect('pick_event', self.add_oval)

        # 添加注释
        self.canvas.mpl_connect('pick_event', self.add_annotation)
        self.canvas.mpl_connect('button_press_event', self.add_annotation)
        self.canvas.mpl_connect('button_release_event', self.add_annotation)
        # 添加文字
        self.canvas.mpl_connect('button_press_event', self.add_text)
        self.canvas.mpl_connect('motion_notify_event', self.add_text)
        self.canvas.mpl_connect('button_release_event', self.add_text)
        self.canvas.mpl_connect('pick_event', self.add_text)
        # 为曲线添加样式的功能实现
        self.canvas.mpl_connect('button_press_event', self.add_style)
        self.canvas.mpl_connect('pick_event', self.add_style)
        # 为图例绑定监听事件
        # self.canvas.mpl_connect('button_press_event', self.change_legend)
        # self.canvas.mpl_connect('pick_event', self.change_legend)
        # 为右键功能集成
        self.canvas.mpl_connect('pick_event', self.right_button_menu)

        # 所有的标志
        self.add_rect_flag = False
        self.add_oval_flag = False
        self.add_text_flag = False
        self.rotate_flag = False
        self.home_flag = False
        self.pan_flag = False
        self.zoom_flag = False
        self.add_style_flag = False
        self.show_grid_flag = False
        self.show_legend_flag = False
        self.add_annotation_flag = False
        # 禁用移动和缩放
        self.toolbar.mode = None
        # 当前的artist对象
        self.current_artist = None
        # 这里面装着可以被home去除的组件
        self.can_remove_elements = []
        # 获取子图对象
        self.axes = self.canvas.figure.get_axes()
        self.current_subplot = None
        self.pick = False
        # 这个button的出现是因为在添加文字时，明明鼠标移动时，没有按下按钮，但是mpl识别出按下了按钮，所以另外做一个标志
        self.button = 0
        # 初始化当前界面
        self.settings_path = os.path.join(os.path.dirname(__file__), 'settings.cfg')
        self.local_font = None
        self.english_font = None
        self.mix_font = None
        self.init_gui()

    def init_gui(self):
        if not self.axes:
            QtWidgets.QMessageBox.warning(
                self.canvas.parent(), "Error", "There are no axes to edit.")
            return
        elif len(self.axes) == 1:
            self.current_subplot, = self.axes
            titles = ['图1']
        else:
            titles = ['图' + str(i + 1) for i in range(len(self.axes))]
            self.current_subplot = self.axes[0]
        # 将三维图的初始视角保存下来，便于旋转之后可以复原
        self.init_views = [(index, item.azim, item.elev) for index, item in enumerate(self.axes) if
                           hasattr(item, 'azim')]
        self.comboBox.addItems(titles)
        # 所有的按钮标志
        self.make_flag_invalid()
        self.default_font()
        self.set_pickers()

    def select_font(self, text: Text):
        content = text.get_text()
        zh_model = re.compile(u'[\u4e00-\u9fa5]')
        en_model = re.compile(u'[a-z]')
        zh = zh_model.search(content)
        en = en_model.search(content)
        if zh and en and self.mix_font!='None' and len(self.font_paths)==len(self.font_names):
            path=self.font_paths[self.font_names.index(self.mix_font)]
            text.set_fontproperties(Path(path))
            return text
        if zh and not en and self.local_font!='None' and len(self.font_paths)==len(self.font_names):
            path=self.font_paths[self.font_names.index(self.local_font)]
            text.set_fontproperties(Path(path))
            return text
        if not zh and self.english_font!='None' and len(self.font_paths)==len(self.font_names):
            path = self.font_paths[self.font_names.index(self.english_font)]
            text.set_fontproperties(Path(path))
            return text
        return text

    def default_font(self):
        config = configparser.ConfigParser()
        config.read(self.settings_path, encoding='utf-8-sig')
        self.english_font,self.local_font,self.mix_font,self.font_paths,self.font_names=['None']*5
        
        if config.has_option('DEFAULT', 'local_font'):
            self.local_font = config.get('DEFAULT', 'local_font')
        if config.has_option('DEFAULT', 'english_font'):
            self.english_font = config.get('DEFAULT', 'english_font')
        if config.has_option('DEFAULT', 'mix_font'):
            self.mix_font = config.get('DEFAULT', 'mix_font')
        if config.has_option('DEFAULT', 'font_paths'):
            self.font_paths = config.get('DEFAULT', 'font_paths')
            self.font_paths=self.font_paths.split(';')
        if config.has_option('DEFAULT', 'font_names'):
            self.font_names = config.get('DEFAULT', 'font_names')
            self.font_names=self.font_names.split(';')

    def set_pickers(self):
        """将所有的对象都设置成可pick的对象，使能被事件响应，同时修改字体等操作"""
        for ax in self.axes:
            ax.set_picker(True)
            for line in ax.lines:
                line.set_picker(True)
            for patch in ax.patches:
                patch.set_picker(True)
            for label in ax.get_xticklabels():  # make the xtick labels pickable
                label.set_picker(True)
            for label in ax.get_yticklabels():  # make the ytick labels pickable
                label.set_picker(True)
            for text in ax.texts:
                self.select_font(text)
                text.set_picker(True)
            self.select_font(ax.title)
            self.select_font(ax.xaxis.label)
            self.select_font(ax.yaxis.label)
            if hasattr(ax, 'zaxis'):
                self.select_font(ax.zaxis.label)
            legend = ax.get_legend()
            if legend:
                for text in legend.texts:
                    self.select_font(text)
                legend.set_picker(True)
                legend.set_draggable(True)
            if hasattr(ax, 'get_zticklabels()'):
                for label in ax.get_zticklabels():
                    label.set_picker(True)

    def make_flag_invalid(self):
        self.add_rect_flag = False
        self.add_oval_flag = False
        self.add_text_flag = False
        self.rotate_flag = False
        self.home_flag = False
        self.pan_flag = False
        self.zoom_flag = False
        self.add_style_flag = False
        self.show_grid_flag = False
        self.show_legend_flag = False
        self.add_annotation_flag = False
        # 禁用移动和缩放
        self.toolbar.mode = None

    def rightMenuShow(self):
        # 创建上下文菜单
        self.contextMenu = QMenu()
        self.actionStyle = self.contextMenu.addAction('修改曲线样式')
        self.actionLegend = self.contextMenu.addAction('修改图例样式')
        self.actionCurve = self.contextMenu.addAction('修改曲线类型')
        self.actionStyle.triggered.connect(self.style_handler)
        self.actionLegend.triggered.connect(self.legend_handler)
        self.actionLegend.triggered.connect(self.curve_handler)

    def home_slot(self):
        self.make_flag_invalid()
        self.home_flag = not self.home_flag
        self.home()

    def home(self):
        """
        matplotlib lines里面放曲线，patches可以放图形，artists也可以放东西，设为空则可以删除对应的对象
        """
        if not self.home_flag:
            return
        # 运行QT5Agg原来的home，实现平移的复位
        self.toolbar.home()
        # 将三维图视角还原
        for item in self.init_views:
            self.axes[item[0]].view_init(azim=item[1], elev=item[2])
        # 将can_remove_elements中的对应的元素全部删除
        for item in self.axes:
            # print(self.can_remove_elements)
            for i in self.can_remove_elements:
                if i in item.texts:
                    item.texts.remove(i)
                if i in item.artists:
                    item.artists.remove(i)
                if i in item.patches:
                    item.patches.remove(i)
                if i in item.lines:
                    item.lines.remove(i)
        self.canvas.draw()

    def zoom_slot(self):
        self.make_flag_invalid()
        self.zoom_flag = not self.zoom_flag
        self.zoom()

    def zoom(self):
        if not self.zoom_flag:
            return
        self.toolbar.zoom()

    def pan_slot(self):
        self.make_flag_invalid()
        self.pan_flag = not self.pan_flag
        self.pan()

    def pan(self):
        if not self.pan_flag:
            return
        self.toolbar.pan()

    def save_slot(self):
        self.toolbar.save_figure()

    def front_slot(self):
        self.toolbar._nav_stack.forward()
        self.toolbar._update_view()

    def back_slot(self):
        self.toolbar._nav_stack.back()
        self.toolbar._update_view()

    def add_text_slot(self):
        self.make_flag_invalid()
        self.add_text_flag = not self.add_text_flag

    def add_text(self, event):
        if not self.add_text_flag:
            return
        if event.name == 'pick_event':
            if type(event.artist) == Text:
                self.current_artist = event.artist
                self.button = 1
        if event.name == 'button_press_event' and event.button == 1 and not type(
                self.current_artist) == Text and event.inaxes and not hasattr(event.inaxes, 'azim'):
            text, ok = QtWidgets.QInputDialog.getText(self.canvas.parent(), '输入文字', '添加注释')
            if ok and text:
                self.current_artist = event.inaxes.text(event.xdata, event.ydata, text)
                self.current_artist.set_picker(True)
                self.can_remove_elements.append(self.current_artist)
                self.canvas.draw()
                self.button = 0
            else:
                return
        if event.name == 'motion_notify_event' and self.button == 1 and event.inaxes and self.current_artist and not hasattr(
                event.inaxes, 'azim'):
            self.current_artist.set_position((event.xdata, event.ydata))
            self.canvas.draw_idle()
        if event.name == 'button_release_event':
            self.current_artist = None

    def rotate_slot(self):
        self.make_flag_invalid()
        self.rotate_flag = not self.rotate_flag

    def on_rotate(self, event):
        """
        通过观察发现，旋转时产生的xdata,ydata是以图像中心为原点，正负0.1范围内的数值
        """
        if not self.rotate_flag:
            return
        # 如果鼠标移动过程有按下，视为拖拽，判断当前子图是否有azim属性来判断当前是否3D
        if event.button and hasattr(event.inaxes, 'azim'):
            for item in self.init_views:
                if self.axes[item[0]] == event.inaxes:
                    delta_azim = 180 * event.xdata * -1 / 0.1
                    delta_elev = 180 * event.ydata / 0.1
                    azim = delta_azim + item[1]
                    elev = delta_elev + item[2]
                    event.inaxes.view_init(azim=azim, elev=elev)
                    self.canvas.draw()

    def add_rect_slot(self):
        # 除了本标记，其余全置False
        self.make_flag_invalid()
        self.add_rect_flag = not self.add_rect_flag

    def add_rect(self, event):
        """
        如果是pick,判断当前对象是否为矩形，不是直接返回，是pick为真
        如果是按下事件，并且当前无pick对象，则创建一个矩形，长宽为0
        如果是移动事件，并且当前pick标志为false，则将矩形长宽改变，否则，改变矩形初始点位置，及拖拽功能
        如果是释放事件，则将pick标志设置为false，artist为None
        """
        if not self.add_rect_flag:
            return
        if event.name == 'pick_event':
            if type(event.artist) == plt.Rectangle:
                self.pick = True
                self.current_artist = event.artist
            else:
                return
        if event.name == 'button_press_event' and event.inaxes and event.button == 1 and not self.pick:
            self.event_init = event
            self.current_artist = plt.Rectangle((self.event_init.xdata, self.event_init.ydata), 0, 0, fill=False,
                                                edgecolor='red', linewidth=3, picker=True)
            event.inaxes.add_artist(self.current_artist)  # 这里不能使用event.inaxes.patches.append方法
            self.can_remove_elements.append(self.current_artist)
            self.canvas.draw()
        if event.name == 'motion_notify_event' and event.button == 1 and event.inaxes and self.current_artist and not self.pick:
            width = event.xdata - self.event_init.xdata
            height = event.ydata - self.event_init.ydata
            center = (self.event_init.xdata + width / 2, self.event_init.ydata + height / 2)
            self.label.setText('Rect Center:(%0.2f,%0.2f) W=%0.2f H=%0.2f S=%0.2f' % (
                center[0], center[1], np.abs(width), np.abs(height), np.abs(width * height)))
            self.current_artist.set_width(width)
            self.current_artist.set_height(height)

            self.canvas.draw()
        if event.name == 'motion_notify_event' and event.button == 1 and event.inaxes and self.current_artist and self.pick:
            width = self.current_artist.get_width()
            height = self.current_artist.get_height()
            center = (event.xdata + width / 2, event.ydata + height / 2)
            self.label.setText('Rect Center:(%0.2f,%0.2f) W=%0.2f H=%0.2f S=%0.2f' % (
                center[0], center[1], np.abs(width), np.abs(height), np.abs(width * height)))
            self.current_artist.set_xy((event.xdata, event.ydata))
            self.canvas.draw()
        if event.name == 'button_release_event':
            self.label.setText('')
            self.current_artist = None
            self.pick = False

    def add_oval_slot(self):
        self.make_flag_invalid()
        self.add_oval_flag = not self.add_oval_flag

    def add_oval(self, event):
        if not self.add_oval_flag:
            return
        if event.name == 'pick_event':
            if type(event.artist) == Ellipse:
                self.pick = True
                self.current_artist = event.artist
            else:
                return
        if event.name == 'button_press_event' and event.inaxes and event.button == 1 and not self.pick:
            self.event_init = event
            self.current_artist = Ellipse(xy=(self.event_init.xdata, self.event_init.ydata),
                                          width=abs(event.xdata - self.event_init.xdata) * 2,
                                          height=abs(event.ydata - self.event_init.ydata) * 2, angle=0, fill=False,
                                          edgecolor='red', linewidth=3, picker=True)
            event.inaxes.add_artist(self.current_artist)  # 这里不能使用event.inaxes.patches.append方法
            self.can_remove_elements.append(self.current_artist)
            self.canvas.draw()
        if event.name == 'motion_notify_event' and event.button == 1 and event.inaxes and self.current_artist and not self.pick:
            width = 2 * (event.xdata - self.event_init.xdata)
            height = 2 * (event.ydata - self.event_init.ydata)
            center = self.current_artist.get_center()
            self.current_artist.set_width(width)
            self.current_artist.set_height(height)
            self.label.setText('Oval:Center:(%0.2f,%0.2f) W=%0.2f H=%0.2f S=%0.2f' % (
                center[0], center[1], np.abs(width), np.abs(height), np.abs(np.pi * width * height / 4)))
            self.canvas.draw()
        if event.name == 'motion_notify_event' and event.button == 1 and event.inaxes and self.current_artist and self.pick:
            width = self.current_artist.get_width()
            height = self.current_artist.get_height()
            center = (event.xdata, event.ydata)
            self.label.setText('Oval:Center:(%0.2f,%0.2f) W=%0.2f H=%0.2f S=%0.2f' % (
                center[0], center[1], np.abs(width), np.abs(height), np.abs(np.pi * width * height / 4)))
            self.current_artist.set_center(center)
            self.canvas.draw()
        if event.name == 'button_release_event':
            self.label.setText('')
            self.current_artist = None
            self.pick = False

    def add_annotation_slot(self):
        self.make_flag_invalid()
        self.add_annotation_flag = not self.add_annotation_flag
        self.annotation = self.current_subplot.annotate('x: %0.2f\ny: %0.2f' % (0, 0),
                                                        xy=(0, 0), xytext=(-20, 20),
                                                        textcoords='offset points', ha='right', va='bottom',
                                                        bbox=dict(boxstyle='round,pad=0.5', fc='yellow', alpha=0.5),
                                                        arrowprops=dict(arrowstyle='->', connectionstyle='arc3,rad=0'),
                                                        picker=True)
        self.point = Line2D([0], [0], ls="",
                            marker='o', markerfacecolor='r',
                            animated=False, pickradius=5, picker=True)
        self.current_subplot.add_artist(self.point)
        self.can_remove_elements.append(self.annotation)
        self.can_remove_elements.append(self.point)
        self.annotation.set_visible(False)
        self.annotation.draggable(True)

    def add_annotation(self, event):
        """
        由于annotation是可拖拽的，所以只需要实现，pick后将annotation的位置调整过去，并且可见，但是这里限制了只能在当前子图内操作
        """
        if not self.add_annotation_flag:
            return
        if event.name == 'pick_event':
            self.current_artist = event.artist
        if event.name == 'button_press_event' and self.current_artist and type(
                self.current_artist) != Annotation and event.inaxes == self.current_subplot:
            x, y = event.xdata, event.ydata
            self.point.set_data(([event.xdata], [event.ydata]))
            self.annotation.xy = x, y
            self.annotation.set_text('x: %0.2f\ny: %0.2f' % (x, y))
            self.annotation.set_visible(True)
            self.canvas.draw()
            self.current_artist = None
        if event.name == 'button_release_event':
            self.current_artist = None

    def add_style_slot(self):
        self.make_flag_invalid()
        self.add_style_flag = not self.add_style_flag

    def add_style(self, event):
        if not self.add_style_flag:
            return
        if event.name == 'pick_event':
            self.current_artist = event.artist
        if self.current_artist and event.name == 'button_press_event':
            for line in event.inaxes.lines:
                if self.current_artist != line:
                    line.set_alpha(0.5)
                else:
                    line.set_alpha(1)
            self.canvas.draw_idle()
            if event.button == 3:
                self.contextMenu.popup(QCursor.pos())  # 2菜单显示的位置
                self.contextMenu.show()
                return
            elif event.button == 1:
                self.current_artist = None
                return
        if not self.current_artist and event.name == 'button_press_event' and event.button == 1:
            for line in event.inaxes.lines:
                line.set_alpha(1)
            self.canvas.draw_idle()

    def show_legend_slot(self):
        if self.show_legend_flag:
            if self.current_subplot.lines:  # 如果存在曲线才允许画图例
                self.current_subplot.legend(frameon=False)
                self.show_legend_flag = False
                self.canvas.draw()
        else:
            legend_titles = []
            for index in range(len(self.current_subplot.lines)):
                legend_titles.append('curve ' + str(index + 1))  # 从1开始算
            if self.current_subplot.lines:  # 如果存在曲线才允许画图例
                leg = self.current_subplot.legend(self.current_subplot.lines, legend_titles)
                leg.set_draggable(True)  # 设置legend可拖拽
                for legline in leg.get_lines():
                    legline.set_pickradius(10)
                    legline.set_picker(True)  # 给每个legend设置可点击
                self.canvas.draw()
                self.show_legend_flag = True

    # def change_legend(self, event):
    #     if not self.show_legend_flag:
    #         return
    #     if event.name == 'pick_event':
    #         self.current_artist = event.artist
    #     if self.current_artist and event.name == 'button_press_event' and event.button == 3:
    #         self.contextMenu.popup(QCursor.pos())  # 2菜单显示的位置
    #         self.contextMenu.show()

    def show_colorbar_slot(self):
        # print(self.current_subplot.curves)
        pass
        # self.canvas.figure.colorbar(self.canvas,self.current_subplot)

    def style_handler(self):
        linestyle_manager.Ui_Dialog_Manager(self.canvas, self.current_artist)
        # print(self.current_artist)
        # self.current_artist.set_linewidth(5)
        # self.canvas.draw_idle()

    def legend_handler(self):
        print(self.current_artist)

    def curve_handler(self):
        print(self.current_artist)

    def mainView_slot(self):
        if hasattr(self.current_subplot, 'azim'):
            self.current_subplot.view_init(azim=0, elev=0)
            self.canvas.draw()

    def leftView_slot(self):
        if hasattr(self.current_subplot, 'azim'):
            self.current_subplot.view_init(azim=90, elev=0)
            self.canvas.draw()

    def rightView_slot(self):
        if hasattr(self.current_subplot, 'azim'):
            self.current_subplot.view_init(azim=-90, elev=0)
            self.canvas.draw()

    def topView_slot(self):
        if hasattr(self.current_subplot, 'azim'):
            self.current_subplot.view_init(azim=0, elev=90)
            self.canvas.draw()

    def bottomView_slot(self):
        if hasattr(self.current_subplot, 'azim'):
            self.current_subplot.view_init(azim=0, elev=-90)
            self.canvas.draw()

    def backView_slot(self):
        if hasattr(self.current_subplot, 'azim'):
            self.current_subplot.view_init(azim=180, elev=0)
            self.canvas.draw()

    def show_grid_slot(self):
        self.show_grid_flag = not self.show_grid_flag
        self.current_subplot.grid(self.show_grid_flag)
        if not self.show_grid_flag:
            self.make_flag_invalid()
        self.canvas.draw_idle()

    def combobox_slot(self):
        self.current_subplot = self.axes[self.comboBox.currentIndex()]  # 将当前选择付给子图对象

    def axes_control_slot(self):
        if not self.current_subplot:
            QtWidgets.QMessageBox.warning(
                self.canvas.parent(), "错误", "没有可选的子图！")
            return
        Ui_Form_Manager(self.current_subplot, self.canvas)
        self.default_font()
        self.set_pickers()
        self.canvas.draw()

    def right_button_menu(self, event):
        """"""
        if event.mouseevent.button == 3:
            if isinstance(event.artist, Legend):
                dialog = QtWidgets.QDialog()
                dialog.setWindowTitle('Legend setting')
                views = []
                for index, text in enumerate(event.artist.get_texts()):
                    views.append((str, 'legend' + str(index), 'legend ' + str(index), text.get_text()))
                choices = ('best', 'upper right', 'upper left', 'lower left', 'lower right', 'right', 'center left',
                           'center right', 'lower center', 'upper center', 'center')
                views.append(('choose_box', 'loc', '位置', 'best', choices))
                views.append((bool, 'shadow', '阴影', event.artist.shadow))
                views.append((bool, 'frameon', '边框', False))

                sp = SettingsPanel(views=views, parent=dialog)

                dialog.setLayout(QHBoxLayout())
                dialog.layout().addWidget(sp)
                dialog.exec_()
                event.mouseevent.button = 0

                lines = event.artist.get_lines()
                view_dict = sp.get_value()
                texts = [view_dict[item] for item in view_dict.keys() if item.startswith('legend')]
                legend = event.mouseevent.inaxes.legend(lines, texts, shadow=view_dict['shadow'],
                                                        frameon=view_dict['frameon'], loc=view_dict['loc'])
                legend.set_picker(True)
                legend.set_draggable(True)
                self.canvas.draw_idle()
            if isinstance(event.artist, Rectangle):
                dialog = QtWidgets.QDialog()
                dialog.setWindowTitle('Rectangle setting')
                views = []
                views.append((str, 'width', 'width', '%.2f' % abs(event.artist.get_width())))
                views.append((str, 'height', 'height', '%.2f' % abs(event.artist.get_height())))
                views.append((str, 'left', 'left coord', '%.2f' % event.artist.get_x()))
                views.append((str, 'bottom', 'bottom coord', '%.2f' % event.artist.get_y()))
                views.append((str, 'linewidth', 'line width', '%.2f' % event.artist.get_linewidth()))
                views.append((bool, 'fill', 'fill', event.artist.get_fill()))
                edgecolor = []
                for item in event.artist.get_edgecolor()[:3]:
                    edgecolor.append(int(255 * item))
                views.append(('color', 'edgecolor', 'edge color', edgecolor))
                facecolor = []
                for item in event.artist.get_facecolor()[:3]:
                    facecolor.append(int(255 * item))
                views.append(('color', 'facecolor', 'face color', facecolor))

                sp = SettingsPanel(views=views, parent=dialog)

                dialog.setLayout(QHBoxLayout())
                dialog.layout().addWidget(sp)
                dialog.exec_()
                event.mouseevent.button = 0

                view_dict = sp.get_value()
                event.artist.set_width(float(view_dict['width']))
                event.artist.set_height(float(view_dict['height']))
                event.artist.set_x(float(view_dict['left']))
                event.artist.set_y(float(view_dict['bottom']))
                event.artist.set_linewidth(float(view_dict['linewidth']))
                event.artist.set_fill(view_dict['fill'])
                event.artist.set_edgecolor(rgb_to_hex(*view_dict['edgecolor']))
                event.artist.set_facecolor(rgb_to_hex(*view_dict['facecolor']))
                self.canvas.draw_idle()
            if isinstance(event.artist, Ellipse):
                dialog = QtWidgets.QDialog()
                dialog.setWindowTitle('Ellipse setting')
                views = []
                views.append((str, 'width', 'width', '%.2f' % abs(event.artist.get_width())))
                views.append((str, 'height', 'height', '%.2f' % abs(event.artist.get_height())))
                views.append((str, 'center', 'center coord', '(%.2f,%.2f)' % event.artist.get_center()))
                views.append((str, 'linewidth', 'line width', '%.2f' % event.artist.get_linewidth()))
                views.append((bool, 'fill', 'fill', event.artist.get_fill()))
                edgecolor = []
                for item in event.artist.get_edgecolor()[:3]:
                    edgecolor.append(int(255 * item))
                views.append(('color', 'edgecolor', 'edge color', edgecolor))
                facecolor = []
                for item in event.artist.get_facecolor()[:3]:
                    facecolor.append(int(255 * item))
                views.append(('color', 'facecolor', 'face color', facecolor))

                sp = SettingsPanel(views=views, parent=dialog)

                dialog.setLayout(QHBoxLayout())
                dialog.layout().addWidget(sp)
                dialog.exec_()
                event.mouseevent.button = 0

                view_dict = sp.get_value()
                event.artist.set_width(float(view_dict['width']))
                event.artist.set_height(float(view_dict['height']))
                event.artist.set_center(tuple(eval(view_dict['center'])))
                event.artist.set_linewidth(float(view_dict['linewidth']))
                event.artist.set_fill(view_dict['fill'])
                event.artist.set_edgecolor(rgb_to_hex(*view_dict['edgecolor']))
                event.artist.set_facecolor(rgb_to_hex(*view_dict['facecolor']))
                self.canvas.draw_idle()
            if isinstance(event.artist, Line2D):
                linestyle_manager.Ui_Dialog_Manager(self.canvas, event.artist)
                # dialog = QtWidgets.QDialog()
                # dialog.setWindowTitle('Line2D setting')
                # views = []
                # views.append((str, 'linewidth', 'line width', str(event.artist.get_linewidth())))
                # views.append(('choose_box', 'linestyle', 'line style', event.artist.get_linestyle(), linestyles))
                # views.append(('choose_box', 'marker', 'marker style', event.artist.get_marker(), markers))
                # views.append((str, 'markersize', 'marker size', str(event.artist.get_markersize())))
                # views.append((str, 'markeredgewidth', 'marker edge width', str(event.artist.get_markeredgewidth())))
                # views.append(('color', 'color', 'color', hex_to_rgb(to_hex(event.artist.get_color()))))
                # views.append(('color', 'markerfacecolor', 'marker face color',
                #               hex_to_rgb(to_hex(event.artist.get_markerfacecolor()))))
                # views.append(('color', 'markeredgecolor', 'marker edge color',
                #               hex_to_rgb(to_hex(event.artist.get_markeredgecolor()))))
                # sp = SettingsPanel(views=views, parent=dialog)
                #
                # dialog.setLayout(QHBoxLayout())
                # dialog.layout().addWidget(sp)
                # dialog.exec_()
                # event.mouseevent.button = 0
                #
                # view_dict = sp.get_value()
                # event.artist.set_linewidth(float(view_dict['linewidth']))
                # event.artist.set_linestyle(view_dict['linestyle'])
                # event.artist.set_marker(view_dict['marker'])
                # event.artist.set_markersize(float(view_dict['markersize']))
                # event.artist.set_color(rgb_to_hex(*view_dict['color']))
                # event.artist.set_markerfacecolor(rgb_to_hex(*view_dict['markerfacecolor']))
                # event.artist.set_markeredgecolor(rgb_to_hex(*view_dict['markeredgecolor']))
                # event.artist.set_markeredgewidth(float(view_dict['markeredgewidth']))
                # self.canvas.draw_idle()
            if isinstance(event.artist, Annotation):
                dialog = QtWidgets.QDialog()
                dialog.setWindowTitle('Annotation setting')
                views = []
                views.append((str, 'text', 'text', event.artist.get_text()))
                views.append((bool, 'show_coord', 'show coord', True))
                views.append((str, 'xy', 'xy position', '(%.2f,%.2f)' % event.artist.xy))
                views.append((str, 'xyann', 'text position', '(%.2f,%.2f)' % event.artist.xyann))
                views.append(
                    ('choose_box', 'arrowstyle', 'arrow style', event.artist.arrowprops['arrowstyle'], arrowstyles))
                sp = SettingsPanel(views=views, parent=dialog)
                dialog.setLayout(QHBoxLayout())
                dialog.layout().addWidget(sp)
                dialog.exec_()
                event.mouseevent.button = 0
                view_dict = sp.get_value()
                if not view_dict['show_coord']:
                    event.artist.set_text(view_dict['text'])
                event.artist.xy = tuple(eval(view_dict['xy']))
                event.artist.xyann = tuple(eval(view_dict['xyann']))
                event.artist.arrowprops = {'arrowstyle': view_dict['arrowstyle']}
                self.canvas.draw_idle()


if QtWidgets.QApplication.instance() is None:
    _pmagg_app = QtWidgets.QApplication(sys.argv) # 没有app，则创建


class FigureManagerPyMiner(FigureManagerBase):
    def show(self):
        from IPython import get_ipython
        ipython = get_ipython()
        if ipython is not None:
            ipython.magic("gui qt5")
            canvas = self.canvas
            self.main = Window(canvas.figure)
            self.main.setWindowTitle('Powered by PyMiner')
            self.main.show()
        else:
            canvas = self.canvas
            self.main = Window(canvas.figure)
            self.main.setWindowTitle('Powered by PyMiner')
            self.main.show()
            _pmagg_app.exec()
        # app.exec()
        # main.callback_pb_load()
        # app.quit()
        # app.exit(app.exec_())
        # sys.exit(app.exec_())


"""下面这堆可以不用管"""


def show(*, block=None):
    """
    For image backends - is not required.
    For GUI backends - show() is usually the last line of a pyplot script and
    tells the backend that it is time to draw.  In interactive mode, this
    should do nothing.
    """
    for manager in Gcf.get_all_fig_managers():
        # do something to display the GUI
        # t = threading.Thread(target=manager.show())
        # t.daemon = True
        # t.start()
        manager.show()
        Gcf.destroy(manager.num)


def draw_if_interactive():
    # 如果setting.cfg存在，且默认字体存在则设置默认字体
    import matplotlib.font_manager as font_manager
    config = configparser.ConfigParser()
    settings_path = os.path.join(os.path.dirname(__file__), 'settings.cfg')
    man = font_manager.FontManager()
    if os.path.exists(settings_path):
        config.read(settings_path, encoding='utf-8-sig')
        if config.has_option('DEFAULT', 'local_font'):
            font = config.get('DEFAULT', 'local_font')
            font_names=config.get('DEFAULT', 'font_names').split(';')
            font_paths=config.get('DEFAULT', 'font_paths').split(';')
            path=font_paths[font_names.index(font)]
            if font!='None' and os.path.exists(path):
                man.addfont(path=path)
                font_name=font.split(':')[0].split(',')[0]
                matplotlib.rcParams['font.sans-serif'] = [font_name]  # 设置简黑字体
                matplotlib.rcParams['axes.unicode_minus'] = False  # 解决‘-’bug
                return
        if config.has_option('DEFAULT', 'mix_font'):
            font = config.get('DEFAULT', 'mix_font')
            font_names=config.get('DEFAULT', 'font_names').split(';')
            font_paths=config.get('DEFAULT', 'font_paths').split(';')
            path=font_paths[font_names.index(font)]
            if font!='None' and os.path.exists(path):
                man.addfont(path=path)
                font_name=font.split(':')[0].split(',')[0]
                matplotlib.rcParams['font.sans-serif'] = [font_name]  # 设置简黑字体
                matplotlib.rcParams['axes.unicode_minus'] = False  # 解决‘-’bug
                return
        if config.has_option('DEFAULT', 'english_font'):
            font = config.get('DEFAULT', 'english_font')
            font_names=config.get('DEFAULT', 'font_names').split(';')
            font_paths=config.get('DEFAULT', 'font_paths').split(';')
            path=font_paths[font_names.index(font)]
            if font!='None' and os.path.exists(path):
                man.addfont(path=path)
                font_name=font.split(':')[0].split(',')[0]
                matplotlib.rcParams['font.sans-serif'] = [font_name]  # 设置简黑字体
                matplotlib.rcParams['axes.unicode_minus'] = False  # 解决‘-’bug
                return


def new_figure_manager(num, *args, FigureClass=Figure, **kwargs):
    """Create a new figure manager instance."""
    # If a main-level app must be created, this (and
    # new_figure_manager_given_figure) is the usual place to do it -- see
    # backend_wx, backend_wxagg and backend_tkagg for examples.  Not all GUIs
    # require explicit instantiation of a main-level app (e.g., backend_gtk3)
    # for pylab.
    thisFig = FigureClass(*args, **kwargs)
    return new_figure_manager_given_figure(num, thisFig)


def new_figure_manager_given_figure(num, figure):
    """Create a new figure manager instance for the given figure."""
    # canvas = FigureCanvasTemplate(figure)
    # manager = FigureManagerTemplate(canvas, num)
    # return manager
    from matplotlib.backends.backend_agg import FigureCanvasAgg
    canvas = FigureCanvasAgg(figure)
    manager = FigureManagerPyMiner(canvas, num)
    return manager
