import os
from typing import Dict, Callable, List
from PyQt5.QtCore import Qt, QModelIndex
from PyQt5.QtGui import QPixmap, QIcon, QCursor
from PyQt5.QtWidgets import QTreeView, QFileSystemModel, QMenu, QVBoxLayout, QWidget, QFileDialog, QPushButton, \
    QHBoxLayout, QSpacerItem, QSizePolicy
extension_lib = None


class RewriteQFileSystemModel(QFileSystemModel):
    def __init__(self, parent=None):
        super().__init__(parent)

    def headerData(self, p_int, Qt_Orientation, role=None):
        if (p_int == 0) and (role == Qt.DisplayRole):
            return self.tr('Name')
        elif (p_int == 1) and (role == Qt.DisplayRole):
            return self.tr('Size')
        elif (p_int == 2) and (role == Qt.DisplayRole):
            return self.tr('Type')
        elif (p_int == 3) and (role == Qt.DisplayRole):
            return self.tr('Last Modified')
        else:
            return super().headerData(p_int, Qt_Orientation, role)

    def columnCount(self, parent: QModelIndex = ...) -> int:
        return 1


class PMFilesTreeview(QTreeView):
    def __init__(self, parent):
        super().__init__(parent)

    def setup_ui(self):
        self.setTabKeyNavigation(True)
        self.setDragEnabled(True)
        self.setDragDropOverwriteMode(True)
        self.setAlternatingRowColors(False)
        self.setUniformRowHeights(True)
        self.setSortingEnabled(True)
        self.setAnimated(True)
        self.setAllColumnsShowFocus(False)
        self.setWordWrap(False)
        self.setHeaderHidden(False)
        self.setObjectName("treeView_files")
        self.header().setSortIndicatorShown(True)

        from pyminer2.extensions.extensionlib.pmext import PluginInterface

        my_dir = PluginInterface.get_work_dir()
        PluginInterface.set_work_dir(my_dir)

        self.model = RewriteQFileSystemModel()
        self.model.extension_lib = self.extension_lib
        self.model.setRootPath(my_dir)
        # self.setRootIndex(self.model.index(QDir.homePath()))

        self.setModel(self.model)
        self.setRootIndex(self.model.index(my_dir))
        self.setAnimated(False)
        self.setSortingEnabled(True)  # 启用排序
        self.header().setSortIndicatorShown(True)  # 启用标题排序
        self.setContextMenuPolicy(Qt.CustomContextMenu)

        self.init_context_menu()

    def bind_events(self, main_window):
        self.openAction.triggered.connect(main_window.openActionHandler)
        self.importAction.triggered.connect(main_window.importActionHandler)
        self.renameAction.triggered.connect(main_window.renameActionHandler)
        self.deleteAction.triggered.connect(main_window.deleteActionHandler)

        self.customContextMenuRequested.connect(self.show_context_menu)

    def init_context_menu(self):
        self.contextMenu = QMenu(self)
        self.openAction = self.contextMenu.addAction(self.tr('Open'))
        self.importAction = self.contextMenu.addAction(self.tr('Import'))
        self.renameAction = self.contextMenu.addAction(self.tr('Rename'))
        self.deleteAction = self.contextMenu.addAction(self.tr('Delete'))

    def show_context_menu(self):
        self.contextMenu.popup(QCursor.pos())
        self.contextMenu.show()


class Stack(object):

    def __init__(self):
        # 创建空列表实现栈
        self.__list = []

    def is_empty(self):
        # 判断是否为空
        return self.__list == []

    def push(self, item):
        # 压栈，添加元素
        self.__list.append(item)

    def pop(self):
        # 弹栈，弹出最后压入栈的元素
        if self.is_empty():
            return
        else:
            return self.__list.pop()

    def top(self):
        # 取最后压入栈的元素
        if self.is_empty():
            return
        else:
            return self.__list[-1]

    def __len__(self):
        return len(self.__list)

    def __str__(self):
        return str(self.__list)


class PMFilesTree(QWidget):
    extension_lib = None
    open_methods_dic: Dict[str, List[Callable]] = {}

    def __init__(self, parent=None):
        global extension_lib
        super().__init__(parent)

        extension_lib = self.extension_lib

    def setup_ui(self):

        from pyminer2.extensions.extensionlib.pmext import PluginInterface
        my_dir = PluginInterface.get_work_dir()

        layout = QVBoxLayout()
        self.file_tree_view = PMFilesTreeview(self)
        self.file_tree_view.extension_lib = self.extension_lib
        self.file_tree_view.setup_ui()
        self.file_tree_view.doubleClicked.connect(
            self.file_item_double_clicked)

        # 定义目录树栈
        self.pre_stack = self.file_tree_view.model.index(my_dir)
        self.root_index = self.file_tree_view.model.index(my_dir)
        hlayout = QHBoxLayout()

        self.pre_dir_button = QPushButton()
        self.pre_dir_button.setToolTip(self.tr("Back"))
        self.pre_dir_button.setFlat(True)
        self.pre_dir_button.setStyleSheet("border: none;")
        self.pre_dir_button.setEnabled(False)
        self.pre_dir_button.clicked.connect(self.pre_index_change)
        icon1 = QIcon()
        icon1.addPixmap(
            QPixmap(":/pyqt/source/images/pre.png"),
            QIcon.Normal,
            QIcon.On)
        self.pre_dir_button.setIcon(icon1)

        path_choose_button = QPushButton()
        path_choose_button.setToolTip(self.tr("Open Path"))
        path_choose_button.setStyleSheet("border: none;")
        icon3 = QIcon()
        icon3.addPixmap(
            QPixmap(":/pyqt/source/images/openfolder.png"),
            QIcon.Normal,
            QIcon.On)
        path_choose_button.setIcon(icon3)
        path_choose_button.clicked.connect(self.on_path_choose_request)

        qspacer = QSpacerItem(20, 20, QSizePolicy.MinimumExpanding, QSizePolicy.Fixed)

        hlayout.addWidget(self.pre_dir_button)
        # hlayout.addWidget(self.next_dir_button)
        hlayout.addWidget(path_choose_button)
        hlayout.addItem(qspacer)
        layout.addLayout(hlayout)
        layout.addWidget(self.file_tree_view)

        self.setLayout(layout)

    def table_change(self, index):
        self.file_tree_view.setRootIndex(index)
        self.pre_stack = index
        self.pre_dir_button.setEnabled(True)
        # if self.pre_stack:
        #     self.pre_dir_button.setEnabled(True)

    def open_file(self, path: str):
        import os
        ext: str = os.path.splitext(path)[1]
        open_methods_for_this_ext = self.open_methods_dic.get(ext)
        if open_methods_for_this_ext is not None:
            for method in open_methods_for_this_ext:
                method(path)
        else:
            os.startfile(path)

    def file_item_double_clicked(self, index: QModelIndex):
        file_info = self.file_tree_view.model.fileInfo(index)
        if self.file_tree_view.model.fileInfo(index).isDir():
            self.table_change(index)
        else:
            self.open_file(file_info.absoluteFilePath())

    def pre_index_change(self):
        if self.pre_stack != self.root_index:
            index = self.pre_stack.parent()
            self.file_tree_view.setRootIndex(index)
            self.pre_stack = self.pre_stack.parent()
            if self.pre_stack == self.root_index:
                self.pre_dir_button.setEnabled(False)
        else:
            self.pre_dir_button.setEnabled(False)

    def on_path_choose_request(self):
        current_work_dir = self.extension_lib.Program.get_work_dir()
        path = QFileDialog.getExistingDirectory(self, '...', current_work_dir)
        if path:
            fsmodel = RewriteQFileSystemModel()
            fsmodel.extension_lib = self.extension_lib
            fsmodel.setRootPath(path)
            self.file_tree_view.setModel(fsmodel)
            self.file_tree_view.model = fsmodel
            self.file_tree_view.setRootIndex(
                self.file_tree_view.model.index(path))
            self.extension_lib.Program.set_work_dir(path)

    def get_split_portion_hint(self):
        return (0.2, None)
